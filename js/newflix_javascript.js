var url = 'https://campus02.fladi.at/web/api-hl/genres/?limit=10';
var genreList = [];

var loadGenre = function(url){
  var req = new XMLHttpRequest();
  req.open('GET', url);
  req.setRequestHeader('Accept', 'application/json');
  req.addEventListener('loadstart', function(){
    console.log("Request wird gesendet");
  });
  req.addEventListener('load', function(){
    console.log("Request erfolgreich!");
    var data = JSON.parse(this.response);
    console.log(data);
    genreList = genreList.concat(data.results);
    if (data.next) {
      loadGenre(data.next);
    } else {
      console.log(genreList);
    }
  });
  req.addEventListener('error', function() {
    console.log("Request fehlgeschlagen!");
  });
  req.send();

};

loadGenre(url);
