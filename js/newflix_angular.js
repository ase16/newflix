angular.module('newflix', [])
.controller('carousel', ['$scope', '$http', function($scope, $http) {
  const movies = 6;
  var d = new Date();
  var date = d.getUTCFullYear() + "-" + (d.getUTCMonth()+1) + "-" + d.getUTCDate();
  var url = 'https://campus02.fladi.at/web/api-hl/movies/?max_released=' + date + '&ordering=-released&limit=' + movies;

  $scope.movies = [];

  $http({
    method: 'GET',
    url: url
  }).then(function(response) {
    $scope.movies = response.data.results;
  });

}]).controller('testinput', ['$scope', function($scope) {
  $scope.text = "bla";

}])
