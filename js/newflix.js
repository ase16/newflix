var app = angular.module('newflix', ['angular-carousel-3d', 'ngSanitize', 'com.2fdevs.videogular']);
app.controller('carousel', ['$scope', '$http', function($scope, $http) {
  var vm = this;
  var movies = 7;
  var d = new Date();
  var date = d.getUTCFullYear() + "-" + (d.getUTCMonth()+1) + "-" + d.getUTCDate();
  var url = 'https://campus02.fladi.at/web/api-hl/movies/?max_released=' +
   date + '&ordering=-released&limit=' + movies;
  vm.movies = [];
  vm.options = {
            visible: 5,
            perspective: 35,
            startSlide: 0,
            border: 3,
            dir: 'rtl',
            width: 360,
            height: 560,
            space: 220,
            autoRotationSpeed: 2500,
            loop: true
        };
  $http({
    method: 'GET',
    url: url
  }).then(function(resp) {
    vm.movies = resp.data.results;
  });
}]);
app.controller('testinput', ['$scope', '$interval', function($scope, $interval) {
  $scope.text = "Hallo";
  $scope.jetzt = new Date();
  var onInterval = function() {
    $scope.jetzt = new Date();
  };
  var int = $interval(onInterval, 1000);

  // Stopuhr
  $scope.counter = 0;
  var onStopuhr = function() {
    $scope.counter++;
  };
  var stopuhr;

  $scope.stopuhrRunning = false;
  $scope.toggle = function(){
    if ($scope.stopuhrRunning) {
      $interval.cancel(stopuhr);
      $scope.stopuhrRunning = false;
    } else {
      $scope.counter = 0;
      stopuhr = $interval(onStopuhr, 1000);
      $scope.stopuhrRunning = true;
    }

  };

}]);
app.directive('moviesearch', ['$http', '$q', function($http, $q) {
  return {
    restrict: 'E',
    //template: '{{ derwert }}: <input name="search" type="text"/>',
    templateUrl: 'moviesearch.html',
    controller: ['$scope', '$element', '$attrs', function($scope, $element, $attrs) {
      var url = 'https://campus02.fladi.at/web/api-hl/movies/?title__icontains=';
      $scope.options = {
        debounce: 300
      };
      var canceler;
      $scope.$watch('title', function(newVal, oldVal) {
        $scope.movies = [];
        if (canceler) {
          canceler.resolve();
        }
        canceler = $q.defer();
        if (newVal === undefined || newVal.length < 3) {
          return;
        }
        console.log(newVal);
        var search = function(url) {
          $http({
            method: 'GET',
            url: url
            //timeout: canceler.promise
          }).then(function(resp) {
            if (newVal === $scope.title) {
              $scope.movies = $scope.movies.concat(resp.data.results);
              if (resp.data.next) {
                search(resp.data.next);
              }
            }
          });
        };
        search(url + newVal);
      });
    }]
  };
}]);

app.directive('rechner', [function() {
  return {
    restrict: 'E',
    templateUrl: 'rechner.html',
    controller: ['$scope', function($scope) {
      var plus = function() {
        $scope.result = $scope.value1 + $scope.value2;
      };
      // $scope.$watch('value1', plus);
      // $scope.$watch('value2', plus);
      $scope.$watchGroup(['value1', 'value2'], plus);
    }]
  };
}]);

app.controller('videoplayer', ['$sce', '$http', '$interval', function($sce, $http, $interval) {
  var position = 0;
  var interval;
  $http.defaults.headers.common['Authorization'] = 'Basic ' + window.btoa('test2016' + ':' + 'test1234');
  this.config = {
    sources: [
      {
        src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.mp4"),
        type: "video/mp4"
      }
    ]
  };
  this.updateTime = function(time, duration) {
    console.log(time, duration);
    position = time;
  };
  this.onComplete = function() {
    console.log("Video fertig!");

    $http({
      method: 'POST',
      url: 'https://campus02.fladi.at/web/api-hl/history/',
      data: {
        movie: 'https://campus02.fladi.at/web/api-hl/movies/151/'
      }
    });
  };
  this.onUpdateState = function(state) {
    console.log(state);
    if (state === "play") {
      interval = $interval(function() {
        console.log("Speichere Zeit " + position);
        $http({
          method: 'POST',
          url: 'https://campus02.fladi.at/web/api-hl/resumes/',
          data: {
            movie: 'https://campus02.fladi.at/web/api-hl/movies/151/',
            position: position.toFixed()
          }
        });
      }, 5000);
    } else {
      $interval.cancel(interval);
    }
  };
}]);
