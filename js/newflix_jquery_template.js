const movies = 69;

var d = new Date();
var date = d.getUTCFullYear() + "-" + (d.getUTCMonth()+1) + "-" + d.getUTCDate();
var carousel = $('#myCarousel');
var indicators = $('.carousel-indicators', carousel);
var inner = $('.carousel-inner', carousel);
var url = 'https://campus02.fladi.at/web/api-hl/movies/?max_released=' +
 date + '&ordering=-released&limit=' + movies;

var req = $.ajax({
  url: url,
  method: 'GET',
  dataType: 'json'
});
req.done(function(msg) {
  console.log(msg);
  msg.results.map(function(movie, index) {
    var li = $('<li data-target="#myCarousel" />');
    li.attr('data-slide-to', index);
    if (index === 0) {
      li.addClass('active');
    }
    indicators.append(li);
    var slide = $('<div class="item"><img /><div class="container"><div class="carousel-caption"><h1></h1><p class="synopsis"></p></div></div></div>');
    $('img', slide).attr('src', movie.poster);
    $('h1', slide).text(movie.title);
    $('p.synopsis', slide).text(movie.synopsis);
    if (index === 0) {
      slide.addClass('active');
    }
    if (movie.homepage) {
      var button = $('<p><a class="btn btn-lg btn-primary" role="button">Homepage</a></p>');
      $('a.btn', button).attr('href', movie.homepage);
      $('.carousel-caption', slide).append(button);
    }
    inner.append(slide);
  });
});
req.fail(function() {

});
