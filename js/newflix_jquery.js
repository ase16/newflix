var d = new Date();
var date = d.getUTCFullYear() + "-" + (d.getUTCMonth()+1) + "-" + d.getUTCDate();
var slides = $('#myCarousel .item');

var url = 'https://campus02.fladi.at/web/api-hl/movies/?max_released=' +
 date + '&ordering=-released&limit=' + slides.length;

var req = $.ajax({
  url: url,
  method: 'GET',
  dataType: 'json'
});
req.done(function(msg) {
  console.log(msg);
  slides.each(function(index, element){
    $('img', element).attr('src', msg.results[index].poster);
    $('h1', element).text(msg.results[index].title);
    $('p.synopsis', element).text(msg.results[index].synopsis);
    if (msg.results[index].homepage) {
      $('a.btn', element).attr('href', msg.results[index].homepage).text('Homepage');
    } else {
      $('a.btn', element).parent().hide();
    }
  });
});
req.fail(function() {

});
